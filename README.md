PyCRTools
----------------------------------------------------------------------------
Standalone version of the PyCRTools. A copy of the old lofar_user software repository can be found also on the Nijmegen [gitlab site](https://gitlab.science.ru.nl/lofar_crksp/lofar_usersoftware_svn_backup)

## Build and Setup
To build PyCRTools use a standard cmake out-of-source build, e.g.

    mkdir build && cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=/some/directory
    nice -n 10 make -j20
    make install

to set the according LD_LIBRARY_PATH, PYTHONPATH and PATH variables use

    source /some/directory/setenv.sh

To automate this, add the lines to your .bash_profile and/or .bashrc:

    source $LOFARSOFT/setenv.sh

## Test
Some tests are available via `ctest` or
    make test
To see the autput in case of failing tests use
    CTEST_OUTPUT_ON_FAILURE=1 make test



## Externals
The following externals are shipped with the pycrtools. See individual folders and git history for version numbers and possible modifications.

* casacore

  Suite of c++ libraries for radio astronomy data processing. From: http://casacore.github.io/casacore

* DAL

  Data Access Library for the LOFAR radio astronomy telescope. From: https://github.com/nextgen-astrodata/DAL
	This ist the latest version for access to data **after XX.YY.2017**

* DAL1

  Data Access Library for the LOFAR radio astronomy telescope. From: https://github.com/nextgen-astrodata/DAL
	This ist the first? version for access to data **before XX.YY.2017**

* num_util

	From: http://usg.lofar.org/svn/code/trunk/external/num_util/src

* tmf

  From: https://github.com/pschella/tmf.git


## Requirements
These additionalpacakges are required, but already installed on the COMA cluster:
 * HDF5
 * h5py



