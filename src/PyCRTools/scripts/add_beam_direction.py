import pycrtools as cr
import pytmf
import numpy as np
import re
import time
try:
    import psycopg2
    have_psycopg2 = True
except ImportError:
    have_psycopg2 = False
    
from optparse import OptionParser
from pycrtools import crdatabase as crdb
import cPickle as pickle

#def pickle_parameter(value):
#        return re.sub("'", '"', pickle.dumps(value))

parser = OptionParser()

parser.add_option("--host", default='coma00.science.ru.nl', help="PostgreSQL host.")
parser.add_option("--user", default='crdb', help="PostgreSQL user.")
parser.add_option("--password", default='crdb', help="PostgreSQL password.")
parser.add_option("--dbname", default='crdb', help="PostgreSQL dbname.")
parser.add_option("-e","--event_id",default=None, help="Update only one event")
parser.add_option("-o","--option",default=None, help="Update only events in the following state")

(options, args) = parser.parse_args()

missing_parsets = []

conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)

# Create cursor
c = conn.cursor()
if options.event_id:
     c.execute("""SELECT e.timestamp, e.eventID, filename, s.stationname FROM
            events AS e
            INNER JOIN eventparameters AS ep ON (e.eventID=ep.eventID)
            INNER JOIN event_datafile AS ed ON (e.eventID=ed.eventID)
            INNER JOIN datafiles AS df ON (ed.datafileID=df.datafileID)
            INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
            INNER JOIN stations as s ON (ds.stationID=s.stationID)
            WHERE (e.eventID={0}) """.format(options.event_id)) 


elif options.option:
     c.execute("""SELECT e.timestamp, e.eventID, filename, s.stationname FROM
            events AS e
            INNER JOIN eventparameters AS ep ON (e.eventID=ep.eventID)
            INNER JOIN event_datafile AS ed ON (e.eventID=ed.eventID)
            INNER JOIN datafiles AS df ON (ed.datafileID=df.datafileID)
            INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
            INNER JOIN stations as s ON (ds.stationID=s.stationID)
            WHERE (e.status='ERROR') """)     

else:
    c.execute("""SELECT e.timestamp, e.eventID, filename, s.stationname FROM
            events AS e
            INNER JOIN eventparameters AS ep ON (e.eventID=ep.eventID)
            INNER JOIN event_datafile AS ed ON (e.eventID=ed.eventID)
            INNER JOIN datafiles AS df ON (ed.datafileID=df.datafileID)
            INNER JOIN datafile_station AS ds ON (ed.datafileID=ds.datafileID)
            INNER JOIN stations as s ON (ds.stationID=s.stationID)
            WHERE (((e.antennaset='HBA_DUAL') OR (e.antennaset='HBA_JOINED') OR (e.antennaset='HBA_DUAL_INNER') OR (e.antennaset='HBA_ZERO') OR (e.antennaset='HBA_ONE'))) """) 

old_ev = 0
for f in c.fetchall():
    
    timestamp = f[0]
    event_id = f[1]   
    filename = f[2]
    run_id = filename.split('_')[0]
    stationname = f[3]

    # Get timing information in correct zone
    time_full = time.gmtime(timestamp)
    Y = time_full[0]
    M = time_full[1]
    D = time_full[2]
    h = time_full[3]
    m = time_full[4]
    s = time_full[5]    
    utc = pytmf.gregorian2jd(Y, M, D + (h + (m + (s / 60.)) / 60.) / 24.)

    # Get parset
    try :
        parset = file('/vol/astro3/lofar/vhecr/lora_triggered/data/parsets/'+run_id+'.parset','r')
#        print 'use '+run_id+'.parset'
    except:
        print "No Parset File available for run ", run_id
        missing_parsets.append(run_id)
        continue
        
    # Finding beam in parset file    
    for line in parset:
        if 'Observation.AnaBeam[0].angle1' in line:
            d = line.split()
            ra_rad = float(d[2])
        if 'Observation.AnaBeam.angle1' in line:
            d = line.split()
            ra_rad = float(d[2])
        if 'Observation.AnaBeam[0].angle2' in line:
            d = line.split()
            dec_rad = float(d[2]) 
        if 'Observation.AnaBeam.angle2' in line:
            d = line.split()
            dec_rad = float(d[2])     
       
                     
#    ----------------------------------------------------
#    Gather information    
    alpha = ra_rad # Right ascention
    delta = dec_rad # Declination
    phi = pytmf.deg2rad(52.915122495) #(LOFAR Superterp)
    L = pytmf.deg2rad(6.869837540)
#
#     Calculate Azimuth, Elevation (result is westwards from north)
    A, h = pytmf.radec2azel(alpha, delta, utc, 0., L, phi)
#        
#     Convert to LOFAR convention    
    A *= -1.
    
    beam = (pytmf.rad2deg(A),pytmf.rad2deg(h))
    beam = crdb.pickle_parameter(beam)
  
    print "Updating ", event_id, stationname   

    c.execute("""   UPDATE stationparameters SET beam_direction='{0}' 
                    FROM datafiles, events, event_datafile, stations, datafile_station 
                    WHERE (events.eventID=event_datafile.eventID AND datafiles.datafileID=event_datafile.datafileID AND datafile_station.datafileID=datafiles.datafileID AND stations.stationID=datafile_station.stationID AND stations.stationID=stationparameters.stationID AND events.eventID='{1}' AND stations.stationname='{2}')""".format(beam,event_id,stationname))



    
conn.commit()
c.close()
conn.close()

print "Finishing with missing parsets for:"
print missing_parsets