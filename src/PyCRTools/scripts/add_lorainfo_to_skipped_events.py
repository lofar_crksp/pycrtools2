import numpy as np
import pycrtools as cr
import re
import psycopg2
import cPickle as pickle
from optparse import OptionParser
import os

from pycrtools import crdatabase as crdb
from pycrtools import lora
from pycrtools import pytmf
import sqlite3

try:
    import psycopg2
    have_psycopg2 = True
except ImportError:
    have_psycopg2 = False

# Connect to database
if have_psycopg2:
    # Open PostgreSQL database
    conn = psycopg2.connect(host='coma00.science.ru.nl', user='crdb', password='crdb', dbname='crdb')
else:
    print 'No database connection!'
    raise
# Get cursor on database
cur = conn.cursor()

sql = "SELECT eventID FROM events WHERE status='SKIPPED'"
cur.execute(sql)
eventlist_SKIPPED = cur.fetchall()

print 'There are %d events: ' % len(eventlist_SKIPPED)
#for e in eventlist:
#print e[0]
eventlist_SKIPPED = [x[0] for x in eventlist_SKIPPED]

dbManager = crdb.CRDatabase("crdb", host="coma00.science.ru.nl", user="crdb", password="crdb", dbname="crdb")
db = dbManager.db

events_patched = []
events_no_lora_info = []
for i, eventid in enumerate(eventlist_SKIPPED):
    #eventid = 212496991
    print 'Reading event data for event %d, id %d ...' % (i, eventid)
    event = crdb.Event(db=db, id=eventid)
    print 'done'

    lorapath = '/vol/astro3/lofar/vhecr/lora_triggered/LORA'
    lora_data = lora.loraInfo(1262304000 + eventid, lorapath)

    if not lora_data:
        print 'NO Lora data found'
        events_no_lora_info.append(eventid)
    else:
        for key in lora_data.keys():
            lora_key = "lora_" + key
            event[lora_key] = lora_data[key]
            #print 'Adding %s: %s' % (key, lora_data[key])
        print 'Write LORA info'
        event.write()
        events_patched.append(eventid)

print 'There were %d events SKIPPED' % len(eventlist_SKIPPED)
print 'Of which %d were patched with LORA info' % len(events_patched)
print 'And %d still have no LORA info' % len(events_no_lora_info)
print ' '
print 'Patched events list:'
print events_patched

