#! /usr/bin/env python

from pycrtools import crdatabase as crdb

from optparse import OptionParser

# Parse commandline options
parser = OptionParser()
parser.add_option("-i", "--id", type="int", help="event ID", default=1)
parser.add_option("--host", default="coma00.science.ru.nl", help="PostgreSQL host.")
parser.add_option("--user", default="crdb", help="PostgreSQL user.")
parser.add_option("--password", default="crdb", help="PostgreSQL password.")
parser.add_option("--dbname", default="crdb", help="PostgreSQL dbname.")
parser.add_option("--release-energy", default=False, action="store_true")
parser.add_option("--release-xmax", default=False, action="store_true")
parser.add_option("--scatter", default=False, action="store_true")
parser.add_option("--energy_lora", default=False, action="store_true")
parser.add_option("--energy_ldf", default=False, action="store_true")
(options, args) = parser.parse_args()

db_filename = options.dbname
dbManager = crdb.CRDatabase("crdb", host=options.host, user=options.user, password=options.password, dbname=options.dbname)
db = dbManager.db

event = crdb.Event(db=db, id=options.id)

if "simulation_cleared_holds" not in event.parameter.keys() or not isinstance(event["simulation_cleared_holds"], list):
    event["simulation_cleared_holds"] = []

cleared = False
if options.release_energy and "energy" not in event["simulation_cleared_holds"]:
    event["simulation_cleared_holds"].append("energy")
    event.simulation_statusmessage = "manually checked, energy released"
    cleared = True

if options.release_xmax and "xmax" not in event["simulation_cleared_holds"]:
    event["simulation_cleared_holds"].append("xmax")
    event.simulation_statusmessage = "manually checked, xmax released"
    cleared = True
    
if options.scatter:
    event.simulation_statusmessage = "manually checked, set to scattered xmax"
    event["simulation_xmax_reason"] = "scatter"
    event["simulation_xmax"] = 650
    cleared = True

if options.energy_lora:
    event["simulation_energy_reason"] = "LDF fit of poor quality, default LORA energy"
    event["simulation_energy"] = event["lora_energy"]

if options.energy_ldf:
    event["simulation_energy_reason"] = "LDF fit after all better than LORA"
    event["simulation_energy"] = event["ldf_fit_energy"]
    cleared = True
    
if cleared:
    event.simulation_status = "CONEX_STARTED"
    
event.write()
