.. _known_issues

============
Known issues
============

This page describes some known issues.

With the cosmic rays analysis pipeline
======================================

The following is a list of known issues with the cosmic rays analysis pipeline.

Galactic noise calibration was off center
----------------------------------------

The relative gain calibration (on the galactic noise) was not centered around 1.0 but rather on roughly 1.1. This was probably due to the fact that the first fit was made with few data points (after a year of running). This needs to be updated. 

The task :class:`~pycrtools.tasks.Galaxy` is used to perform the calibration based on a two component Fourier transform fit to the data that was calculated from the *antennas_cleaned_power* parameter of the :class:`~pycrtools.tasks.FindRFI`. 

The parameters have been updated and the data needs to be reprocessed. It should be checked whether this fix, actually solves the problem. 

Wavefront does not run for new data
----------------------------------------

Due to changes at LOFAR, the timing offsets between stations changed. Thus, the wavefront does not run reliably for newer data. This needs to be fixed in the wavefront task. 


Axes labels are not correct
----------------------------------------

After the implementation of the absolute calibration, not all plots are fixed to have the proper axes labels. This needs to be fixed, otherwise the output of the pipeline diagnostics page does not make any sense. 
The data in the database is fine. All values should be in SI units now. 

Absolute calibration
----------------------------------------

Is implemented and debugged once. In-depth debugging might be needed. Results should always be handed with care. 


Simulation pipeline
----------------------------------------

The simulation pipeline puts events on hold with an Xmax or energy that is out of a given range. This is also the case for manually inserted values of the energy or Xmax. This will leave events indefinitely on hold.  


Uncertainties of the Stokes parameters
----------------------------------------

They way that the uncertainties (on Stokes parameters and polarization angle) are calculated in :class:`~pycrtools.tasks.polarization` is partly wrong. They are not used in our publications. The Monte Carlo that is made is correct, it is however not stored in the database.  
