.. _plotting:


Basic plotting
==============

In order to plot the data we use the :mod:`matplotlib` package. This
can be loaded by using the following command:

.. code-block:: python

   import matplotlib.pyplot as plt

however, this should have already been done when loading the
:mod:`pycrtools`.
Besides the plotting commands provided by :mod:`matplotlib`, this plotting
functionality is also available from the :class:`hArrays`.


Matplotlib
----------

Depending on the system you are working on, you may have to create a
window in which the plots are shown. This is done by:

.. code-block:: python

   plt.show()

Now we can use the following plotting commands to show the average
spectra for two antennas:

.. literalinclude:: plots/time_vs_e-field.py
   :start-after: # Create 1st plot
   :end-before: # Prepare block

Note that the array entries need to be converted to vectors, using the
:func:`vec` method, when they are given as arguments to the plotting
command.

To plot the time series of a section of the dataset, we first read in the
samples from all antennas:

.. literalinclude:: plots/time_vs_e-field.py
   :start-after: # Prepare block
   :end-before: # Create 2nd plot

and then we plot it:

.. literalinclude:: plots/time_vs_e-field.py
   :start-after: # Create 2nd plot

The results of the above is shown below.

.. plot:: tutorial/plots/time_vs_e-field.py

If you want to plot a linear plot use :func:`plt.plot`, for a loglog
plot you use :func:`plt.loglog` and for a log-linear plot you can use
:func:`plt.semilogx` or :func:`plt.semilogy`, depending on which axis
needs to be logarithmic.


Plotting using the :class:`hArray` plotting methods
---------------------------------------------------

Another way of producing plots is to use the plot method of the
:class:`hArray` class:

.. literalinclude:: plots/avspectrum.py
   :start-after: # Plotting

.. plot:: tutorial/plots/avspectrum.py

This creates a semilog-plot with appropriate labels and units (if
provided beforehand).

You can either provide the parameters directly (has precedence), or
set the plotting parameters and attributes to the :class:`par` class
of the array, e.g.::

   >>> array.par.xvalues = x_vector
   >>> array.plot()

If the array is in looping mode, multiple curves are plotted in a
single window. Hence,::

   >>> avspectrum.par.logplot = "y"
   >>> avspectrum[...].plot(legend=datafile.antennas)

.. [resulting plot of the code above]

will simply plot all spectra of all antennas (= highest array index)
in the array.

The available parameters, used in the :class:`hArray.par` class, are:

  =============== ==================================================
  ``xvalues``     An array with corresponding x values.
                  If ``None``, integer numbers from 0 to the length of the array are used.
  ``xlabel``      The x-axis label.
                  If not specified, use the ``name`` keyword of the array.
                  Units are added automatically.
  ``ylabel``      The y-axis label.
                  If not specified, use the ``name`` keyword of the array.
                  Units are added automatically.
  ``xlim``        Tuple with minimum and maximum values for the x-axis.
  ``ylim``        Tuple with minimum and maximum values for the y-axis.
  ``title``       A title for the plot
  ``clf``         If ``True`` (default) clear the screen before plotting.
                  If ``False`` plots are composed with multiple lines from different arrays.
  ``logplot``     Can be used to make log-log or semi-log plots:

                  * "x" -> semilog plot in x
                  * "y" -> semilog plot in y
                  * "xy" -> loglog plot
  =============== ==================================================
