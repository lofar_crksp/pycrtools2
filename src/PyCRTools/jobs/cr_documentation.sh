#! /bin/bash
#SBATCH --output=$HOME/cr_documentation.log
#SBATCH --time=00:10:00
#SBATCH -p short

hostname

export LOFARSOFT=/vol/optcoma/lofarsoft
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

cd /opt/lofarsoft/src/PyCRTools/documentation

make html

rsync -av ./build/html/ /www/astro/live/htdocs/software/pycrtools/

