#! /bin/bash

export LOFARSOFT=/vol/optcoma/lofarsoft
export PYTHONPATH=$LOFARSOFT/release/lib/python:$PYTHONPATH
export LD_LIBRARY_PATH=$LOFARSOFT/release/lib:$LD_LIBRARY_PATH

PYCRTOOLS=$LOFARSOFT/src/PyCRTools

cd /vol/astro3/lofar/sim/pipeline/jobs

/usr/bin/python /opt/lofarsoft/src/PyCRTools/extras/run_cr_simulations.py --host coma00.science.ru.nl --user crdb --password crdb --dbname crdb

