"""
Module documentation
====================

Hyperbolic-Wavefront Task: produces the shape of the incoming wavefront, using pulse arrival times at each antenna.
Stripped version of Wavefront Task; this version only calculates the best-fitting hyperbolic wavefront and az/el direction,
given the eventID, arrival times, arrival time uncertainties, antenna positions, station names per antenna, and a shower core position (x and y) as input.

.. moduleauthor:: Arthur Corstanje <a.corstanje@astro.ru.nl>

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

from scipy.optimize import fmin, leastsq, brute

import pycrtools as cr
from pycrtools.tasks import Task
from pycrtools.tasks import directionfitplanewave
from pycrtools.tasks import loraanalysis
from pycrtools.tasks import shower

import pytmf
import datetime

# Constants
c = 299792458.0

def hyperbolic_function_with_a_fixed(x, a):
    #A = a[0] * 1.0e9 / c # how does the unit conversion work?
    A = 3.0 # ns
    B = a[1] * 1.0e9 / c
    return -A + np.sqrt(A**2 + B**2 * x**2) + a[2]

def hyperbolic_function(x, a):
    A = a[0] * 1.0e9 / c # how does the unit conversion work?
    B = a[1] * 1.0e9 / c
    return -A + np.sqrt(A**2 + B**2 * x**2) + a[2]


# Functions for calling with leastsq
def _general_function(params, xdata, ydata, function):
    return function(xdata, params) - ydata

def _weighted_general_function(params, xdata, ydata, function, weights, verbose=0):
    fit = function(xdata, params)
    outarray = weights * (function(xdata, params) - ydata)  # - avg) # Don't do offset correction
    if params[0] < 0:  # Force positive a for hyperbola
        outarray.fill(1.0e9)
    if (len(params) == 3) and (params[1] < 0):  # Also force hyperbola's b (slope asymptote) to be positive
        outarray.fill(1.0e9)

    return outarray

# Projected distances from shower axis, projected pulse delays. Given: az/el, core (x,y).
def getShowerPlaneDistancesAndDelays(az, el, core, antenna_positions, pulse_delays):
# Given az, el in radians. Core (x, y, z) position with z = 0. Antenna positions 2D array, pulse delays per antenna.
# Returns projected antenna distances to shower axis; projected pulse delays in ns.
    cartesianDirection = - np.array([np.cos(el) * np.sin(az), np.cos(el) * np.cos(az), np.sin(el)])  # minus sign for incoming vector!
    axisDistance = []
    projectionDelay = []

    for pos in antenna_positions:
        relpos = pos - core
        delay = (1 / c) * np.dot(cartesianDirection, relpos)
        distance = np.linalg.norm(relpos - np.dot(cartesianDirection, relpos) * cartesianDirection)
        axisDistance.append(distance)
        projectionDelay.append(delay)

    axisDistance = np.array(axisDistance)
    projectionDelay = np.array(projectionDelay)

    shower_plane_pulse_delays = 1e9 * (pulse_delays - projectionDelay)  # subtract geometric delay from projected to real antenna position

    return (axisDistance, shower_plane_pulse_delays)

def getStationListAndIndicesFromFlatArray(station_names_flat):
    stationList = []
    stationStartIndex = []
    thisStation = ''
    i = 0
    for name in station_names_flat:
        if name != thisStation:
            stationList.append(name)
            thisStation = name
            stationStartIndex.append(i)
        i += 1
    stationStartIndex.append(i)

    return (stationList, stationStartIndex)


class HypWavefront(Task):

    """Obtain wavefront shape.
    """

    parameters = dict(
        eventID=dict(default=None, doc="Event ID, used to infer date to compare with common clock installation"),
        pulse_delays=dict(default=None,
            doc="Pulse arrival times per antenna."),
        pulse_delay_uncertainties=dict(default=None,
            doc="Pulse arrival time uncertainties per antenna."),
        antenna_positions=dict(default=None,
            doc="Antenna positions array in [Nx3] format (2D)."),
        station_names=dict(default=None,
            doc="List of station names."),
        shower_core=dict(default=np.array([0., 0., 0.]), # shower core input
            doc="Core estimate input (e.g. from LDF-fit or particle detector)."),
        fix_parameter_a_Lopes=dict(default=False,
            doc="Fix curvature parameter a to 3 ns == 0.9 m, as in Lopes analysis."),
        save_plots=dict(default=False,
            doc="Store plots"),
        plot_prefix=dict(default="",
            doc="Prefix for plots"),
        plotlist=dict(default=[],
            doc="List of plots"),
        plot_type=dict(default=lambda self: "pdf" if self.plot_publishable else "png",
            doc="Plot type (e.g. png, jpeg, pdf)"),
        plot_title=dict(default=lambda self: not self.plot_publishable,
            doc="Plot title, turn off for publication ready figures."),
        plot_colormap=dict(default="cubehelix",
            doc="Colormap for plots."),
        plot_publishable=dict(default=True,
            doc="Create publishable version of plots."),
        verbose=dict(default=0,
            doc="Verbosity level."),
        # Output parameters:
        wavefront_output=dict(default=None, output=True, # remove?
            doc="Output dictionary for fit parameters."),
        fitParams=dict(default=None, output=True,
            doc="Output calculated fit parameters."),
        fitParamErrors=dict(default=None, output=True,
            doc="Output calculated fit parameter uncertainties."),
        fitAzEl=dict(default=None, output=True,
            doc="Output best-fitting direction az, el."),
        fitChi2=dict(default=None, output=True,
            doc="Fit chi-squared value (reduced)")
        )

    def plotFitCurveWithData(self, axisDistance, shower_plane_pulse_delays, pulse_delay_uncertainties, fit_values, stationList=None, stationStartIndex=None, in_plot_description=None):
        if self.verbose > 0:
            print 'Plotting figure'
        fig = plt.figure()
        plt.subplots_adjust(hspace=0.001)

        ax1 = plt.subplot2grid((4, 1), (0, 0), rowspan=3)
        ax2 = plt.subplot2grid((4, 1), (3, 0), sharex=ax1)

        xticklabels = ax1.get_xticklabels()
        plt.setp(xticklabels, visible=False)

        start = 0
        if self.plot_publishable:
            colors = ['b'] * 24  # don't want to run out of colors array
        else:
            colors = ['b', 'g', 'r', 'c', 'm', 'SaddleBrown', 'k', 'Brown', 'FireBrick'] * 4

        if stationList is not None:
            for i in range(len(stationList)):
                start = stationStartIndex[i]
                end = stationStartIndex[i + 1]
                ax1.errorbar(axisDistance[start:end], shower_plane_pulse_delays[start:end], yerr=pulse_delay_uncertainties[start:end], xerr=None, label=stationList[i], c=colors[i], marker='+', linestyle=' ')
                ax2.plot(axisDistance[start:end], (shower_plane_pulse_delays[start:end] - fit_values[start:end]) / pulse_delay_uncertainties[start:end], c=colors[i], marker='+', linestyle='')
        else:
                ax1.errorbar(axisDistance, shower_plane_pulse_delays, yerr=pulse_delay_uncertainties, xerr=None, label='data', c=colors[0], marker='+', linestyle=' ')
                ax2.plot(axisDistance, (shower_plane_pulse_delays - fit_values) / pulse_delay_uncertainties, c=colors[0], marker='+', linestyle=' ')

        sortingIndices = np.argsort(axisDistance)
        ax1.plot(axisDistance[sortingIndices], fit_values[sortingIndices], lw=2, c='r')
        ax2.plot(axisDistance[sortingIndices], np.zeros(len(sortingIndices)), lw=2, c='r')

        ax1.yaxis.set_major_locator(MaxNLocator(prune='lower'))

        maxX = 50 * int(max(axisDistance) / 50) + 50
        ax1.set_xlim([0.0, maxX])
        if not self.plot_publishable:
            ax1.legend(loc='upper left')

        ax2.set_xlabel('Distance from axis [m]')
        ax2.set_ylabel(r'$\Delta t / \sigma$')
        ax1.set_ylabel('Arrival time in shower plane [ns]')

        if in_plot_description is not None:
            ax1.text(0.95, 0.05, in_plot_description, horizontalalignment='right', verticalalignment='bottom', transform=ax1.transAxes)

        return fig

    # Make in-plot text describing fitted curve parameters, uncertainty, and chi-squared
    def makePlotDescriptionText(self, chi_squared, ndf, fitted_params, fitted_param_errors):
        if not self.plot_publishable:
            fitDescription = "$\\mathrm{{hyperbolic\\, fit:}}\\, "
        else:
            fitDescription = "$"
        figtext=fitDescription+"\\chi^2/ \\mathrm{{ndf}} = {4:.0f} / {5}$\n $a = {0:.2f} \\pm {1:.2f},\\, b = {2:.4f} \\pm {3:.3}$".format(fitted_params[0], fitted_param_errors[0], fitted_params[1], fitted_param_errors[1], chi_squared, ndf)

        return figtext


    # Given core, az, el: do curve fit of type fitType to wavefront in shower coordinates.
    def fit_chi2_and_curve_params(self, core, az, el, antenna_positions, pulse_delays, uncertainties=1.0, start_params=None, verbose=0):

        # Get projected distances and delays (shower plane)
        (axisDistance, shower_plane_pulse_delays) = getShowerPlaneDistancesAndDelays(az, el, core, antenna_positions, pulse_delays)
        if start_params is None:
            start_params = [0.9, 0.03, 0.0] # hyperbolic start parameters

        func = _weighted_general_function

        shower_plane_pulse_delays -= shower_plane_pulse_delays.min()

        if self.fix_parameter_a_Lopes:
            fit_function = hyperbolic_function_with_a_fixed
        else:
            fit_function = hyperbolic_function

        res = leastsq(func, start_params, args=(axisDistance, shower_plane_pulse_delays, fit_function, 1. / uncertainties, verbose), full_output=True)
        fitted_params, pcov = res[0], res[1]

        # Regular chi^2, not reduced (for equal weight of points in particle fit)
        chi2 = (func(fitted_params, axisDistance, shower_plane_pulse_delays, fit_function, 1. / uncertainties, verbose) ** 2).sum()

        chi2_radio = chi2
        ndf_radio = len(axisDistance) - len(start_params)

        if pcov is not None:
            pcov = pcov * chi2_radio / ndf_radio
            fitted_param_errors = [np.sqrt(pcov[i][i]) for i in range(len(start_params))]

            if verbose >= 1:
                # But print reduced chi^2
                print "Chi^2 / ndf", chi2_radio / ndf_radio
                print 'Best parameters are: a x + b; a = %3.3f, b = %3.3f' % (fitted_params[0], 0.0)  # fitted_params[1])
                print 'Error a = %3.6f, b = %3.6f' % (np.sqrt(pcov[0][0]), -1)  # np.sqrt(pcov[1][1]))

        else:
            if verbose >= 1:
                print 'WARNING: singular cov matrix, no error estimate possible! Taking error = values'
            fitted_param_errors = list(fitted_params)

        fit_values = fit_function(axisDistance, fitted_params) - fitted_params[-1]
        shower_plane_pulse_delays -= fitted_params[-1]

        return (chi2, chi2_radio, ndf_radio, axisDistance, shower_plane_pulse_delays, fitted_params, fit_values, fitted_param_errors)

    # Minimizer function for optimizing az/el. Does a curve fit for every az/el step.
    # Could be merged into leastsq call, but like this may be safer (and slower).
    def chi2Minimizer_azel(self, azel, core, positions2D, times, uncertainties=1.0, start_params=None, verbose=0):
        (az, el) = azel
        (chi2, chi2_radio, ndf_radio, axisDistance, shower_plane_pulse_delays, fitted_params, fit_values, fitted_param_errors) = self.fit_chi2_and_curve_params(core, az, el, positions2D, times, uncertainties=uncertainties, start_params=start_params, verbose=False)
        if verbose >= 2:
            print 'Evaluated direction: az = %3.3f, el = %2.3f, chi^2 = %1.4f' % (pytmf.rad2deg(az), pytmf.rad2deg(el), chi2_radio / ndf_radio)

        return chi2


    def save_plot(self, fig, plotname):
        if self.save_plots:
            p = self.plot_prefix + plotname + ".{0}".format(self.plot_type)
            print p
            fig.savefig(p)
            self.plotlist.append(p)
        else:
            pass


    def shiftStationPulseDelays(self, pulseDelays, stationNames, station, shiftInNs):
        for i, thisStation in enumerate(stationNames):
            if thisStation == station:
                pulseDelays[i] += shiftInNs * 1e-9


    def run(self):
        """Run the task.
        """
        outer_stations = ['CS011', 'CS013', 'CS017', 'CS021', 'CS024', 'CS026', 'CS028', 'CS030', 'CS031', 'CS032']

        # Sort input data on station list, ascending
        sortingIndices = np.argsort(self.station_names, kind='mergesort') # Mergesort preserves order for equal names
        self.pulse_delays = self.pulse_delays[sortingIndices]
        self.antenna_positions = self.antenna_positions[sortingIndices]
        self.pulse_delay_uncertainties = self.pulse_delay_uncertainties[sortingIndices]
        self.station_names = self.station_names[sortingIndices]
        # Hack to fix calibration of CS011 in event 62798482
        if self.eventID == 62798482:
            self.shiftStationPulseDelays(self.pulse_delays, self.station_names, 'CS011', -2.5)
            print 'WARNING: Hacked CS011 offset for this event'

        if self.eventID == 61593274:
#            self.shiftStationPulseDelays(self.pulse_delays, self.station_names, 'CS0', -2.5)
            selectOutCS006 = np.argwhere(self.station_names != 'CS006').ravel()
            self.pulse_delays = self.pulse_delays[selectOutCS006].copy()
            self.station_names = self.station_names[selectOutCS006].copy()
            self.antenna_positions = self.antenna_positions[selectOutCS006].copy()
            self.pulse_delay_uncertainties = self.pulse_delay_uncertainties[selectOutCS006].copy()
            print 'WARNING: Removed CS006 for this event'

        if self.eventID == 112665116:
#            self.shiftStationPulseDelays(self.pulse_delays, self.station_names, 'CS0', -2.5)
            selectOutCS013 = np.argwhere(self.station_names != 'CS013').ravel()
            self.pulse_delays = self.pulse_delays[selectOutCS013].copy()
            self.station_names = self.station_names[selectOutCS013].copy()
            self.antenna_positions = self.antenna_positions[selectOutCS013].copy()
            self.pulse_delay_uncertainties = self.pulse_delay_uncertainties[selectOutCS013].copy()
            print 'WARNING: Removed CS013 for this event'

        if self.eventID == 73285221:
            self.shiftStationPulseDelays(self.pulse_delays, self.station_names, 'CS011', +5.0)
            self.shiftStationPulseDelays(self.pulse_delays, self.station_names, 'CS017', -2.5)


        # Fix CS007 clock offsets for selected antennas
        # select out CS007
        if self.eventID <= 72094854 and 'CS007' in self.station_names:
            selectCS007 = np.argwhere(self.station_names == 'CS007').ravel()
            startCS007 = selectCS007.min()
            subsetPulseDelays = self.pulse_delays[selectCS007].copy()
            subsetStationNames = self.station_names[selectCS007].copy()
            subsetAntennaPositions = self.antenna_positions[selectCS007].copy()
            subsetPulseDelayUncertainties = self.pulse_delay_uncertainties[selectCS007].copy()
#            import pdb; pdb.set_trace()
#            print 'Sum = %f' % subsetPulseDelays.sum()
            direction_fit_plane_wave = cr.trun("DirectionFitPlaneWave", positions=subsetAntennaPositions, timelags=subsetPulseDelays, verbose=True, ignoreNonHorizontalArray=True) # Array with outer stations is nonhorizontal, ignore z > 0
            residual_delays = 1.0e9 * direction_fit_plane_wave.residual_delays.toNumpy()
#            print 'Sum = %f' % subsetPulseDelays.sum()

            plt.figure()
            plt.plot(residual_delays)
    #        med = residual_delays.max() - residual_delays.min()
    #        med /= 2
            med = np.median(residual_delays)
            if self.verbose > 0:
                print np.median(residual_delays)
            indicesToShift = np.argwhere(residual_delays < med).ravel() # Shift UP when it's too low (not other way round)
            residual_delays[residual_delays < med] += 5.0
            subsetPulseDelays[indicesToShift] += 5.0e-9
            plt.plot(residual_delays)
            # Additionally, require times to be < +/- 3 ns from planewave mean
            med2 = np.median(residual_delays)
            indicesToShiftUp = np.argwhere(residual_delays - med2 < -3.0).ravel()
            indicesToShiftDown = np.argwhere(residual_delays - med2 > +3.0).ravel()
            if self.verbose > 0:
                print indicesToShift
            for index in indicesToShift:
                if self.verbose > 0:
                    print 'Changing %3.3f into %3.3f' % (1e9*self.pulse_delays[index + startCS007], 1e9*self.pulse_delays[index+startCS007] + 5.0)
                self.pulse_delays[index + startCS007] += 5.0e-9
            for index in indicesToShiftUp:
                self.pulse_delays[index + startCS007] += 5.0e-9
            for index in indicesToShiftDown:
                self.pulse_delays[index + startCS007] -= 5.0e-9
        # End CS007 offset fix

        # All parameters that go into the output dictionary
        fitParams = {}
        fitParamErrors = {}
        fitChi2 = {}
        fitAzEl = {}
        d_axisDistance = {}
        d_showerPlanePulseDelays = {}
        d_pulseDelayUncertainties = {}
        d_evaluatedFitValues = {}
        fitParams = {}
        fitParamErrors = {}
        fitChi2 = {}
        fitAzEl = {}
        d_axisDistance = {}
        d_showerPlanePulseDelays = {}
        d_pulseDelayUncertainties = {}
        d_evaluatedFitValues = {}

        self.pulse_delay_uncertainties *= 1.0e9  # in ns.
        #self.pulse_delay_uncertainties = np.sqrt(0.3**2 + self.pulse_delay_uncertainties**2)
        # Get list of stations and their start index in the antenna arrays
        (stationList, stationStartIndex) = getStationListAndIndicesFromFlatArray(self.station_names)
        # Plane wave fit to arrival times
        direction_fit_plane_wave = cr.trun("DirectionFitPlaneWave", positions=self.antenna_positions, timelags=self.pulse_delays, verbose=True, ignoreNonHorizontalArray=True) # Array with outer stations is nonhorizontal, ignore z > 0
        residual_delays = direction_fit_plane_wave.residual_delays.toNumpy()

        (plane_az, plane_el) = direction_fit_plane_wave.meandirection_azel
        print 'From plane wave fit: az = %3.2f, el = %3.2f' % (pytmf.rad2deg(plane_az), pytmf.rad2deg(plane_el))

        # If needed, do an incremental trial-and-error search for the correct outer station clock offsets
        # For dates < 10 Oct 2012: CS011 and higher have an offset that's calibrated using CalibrateFM.
        # with the result read in in cr_physics.py. Ambiguity +/- {-2, -1, 0, 1, 2} * 11.364 ns needs to be checked here.
        clockOffsetSearch = {}
        shiftsPerStation = {}
        if datetime.datetime(2010, 1, 1) + datetime.timedelta(seconds = self.eventID) < datetime.datetime(2012, 10, 10):
            stationsToCorrect = [station for station in outer_stations if station in stationList]
            print 'Doing clock offset trial search on stations: '
            print stationsToCorrect
            # Incremental or just adding one station to the superterp every time?? Now incremental.
            for station in stationsToCorrect:
                clockOffsetSearch[station] = {}
                # Stations are sorted on input (done above)
                # Restrict stations up to (incl) this station
                i = 1 + np.argwhere(self.station_names == station).max() # Up to incl. current station
                subsetPulseDelays = self.pulse_delays[0:i].copy()
                subsetStationNames = self.station_names[0:i].copy()
                subsetAntennaPositions = self.antenna_positions[0:i].copy()
                subsetPulseDelayUncertainties = self.pulse_delay_uncertainties[0:i].copy()
                # Apply shifts found so far
                #for shiftThisStation in shiftsPerStation.keys():
                #    self.shiftStationPulseDelays(subsetPulseDelays, subsetStationNames, shiftThisStation, shiftsPerStation[shiftThisStation])

                minChi2 = 1.0e9
                bestShift = 0.0
                for shift in [-2*11.3636, -11.3636, 0.0, +11.3636, +2*11.3636, +3*11.363636]:
                    self.shiftStationPulseDelays(subsetPulseDelays, subsetStationNames, station, shift)
                    # Use LORA core
                    optimum = fmin(self.chi2Minimizer_azel, (plane_az, plane_el), (self.shower_core, subsetAntennaPositions, subsetPulseDelays, subsetPulseDelayUncertainties, None, self.verbose), xtol=1e-5, ftol=1e-5, full_output=True)

                    # Get optimal curve fit
                    (bestAz, bestEl) = optimum[0]
                    (chi2, chi2_radio, ndf_radio, axisDistance, shower_plane_pulse_delays, fitted_params, fit_values, fitted_param_errors) = self.fit_chi2_and_curve_params(self.shower_core, bestAz, bestEl, subsetAntennaPositions, subsetPulseDelays, uncertainties=subsetPulseDelayUncertainties, verbose=self.verbose)

                    if chi2_radio / ndf_radio < minChi2:
                        minChi2 = chi2_radio / ndf_radio
                        bestShift = shift
                    print 'Station %s, shift = %2.3f: chi^2 / ndf = %4.3f' % (station, shift, chi2_radio / ndf_radio)
                    clockOffsetSearch[station][shift] = chi2_radio / ndf_radio
                    # Shift back
                    self.shiftStationPulseDelays(subsetPulseDelays, subsetStationNames, station, - shift)
                # end for (shift)
                shiftsPerStation[station] = bestShift
                # Apply best shift to original data before continuing with next station
                print 'SHIFTING %s by %f' % (station, shiftsPerStation[station])
                self.shiftStationPulseDelays(self.pulse_delays, self.station_names, station, shiftsPerStation[station])
            # end for (station)
            # Now apply the best shifts to original data before the analysis
#            for station in shiftsPerStation.keys():

        # Redo direction fit plane wave + plot, in case the clock offsets have changed.
        direction_fit_plane_wave = cr.trun("DirectionFitPlaneWave", positions=self.antenna_positions, timelags=self.pulse_delays, verbose=True, ignoreNonHorizontalArray=True) # Array with outer stations is nonhorizontal, ignore z > 0
        residual_delays = direction_fit_plane_wave.residual_delays.toNumpy()
        # Difference between measured arrival times and plane wave fit. No shower core needed for this.
        # Plots get saved automatically in task call.
        signals = np.copy(self.pulse_delays)
        signals.fill(2.71)  # signal power not used here; do not give all 1.0 as the log is taken.
        if self.save_plots:

            # Don't use background when there are stations outside the superterp
            if len(set(self.station_names) - set(['CS002', 'CS003', 'CS004', 'CS005', 'CS006', 'CS007'])) > 0:
                footprint_use_background = False
            else:
                footprint_use_background = True
            fptask_delta = cr.trerun("Shower", "3", positions=self.antenna_positions, signals=signals, timelags=residual_delays, footprint_colormap=self.plot_colormap, footprint_enable=True, footprint_shower_enable=True, footprint_use_title=self.plot_title, core=self.shower_core, direction=direction_fit_plane_wave.meandirection_azel_deg, save_plots=True, plot_prefix=self.plot_prefix + 'wavefront-curvature-', plot_type=self.plot_type, plotlist=self.plotlist, footprint_use_background=footprint_use_background)
# Use for non-residual arrival time plot            fptask_delta = cr.trerun("Shower", "3", positions=self.antenna_positions, signals=signals, timelags=self.pulse_delays, footprint_colormap=self.plot_colormap, footprint_enable=True, footprint_shower_enable=True, footprint_use_title=self.plot_title, core=self.particle_core, direction=direction_fit_plane_wave.meandirection_azel_deg, save_plots=True, plot_prefix=self.plot_prefix + 'wavefront-curvature-', plot_type=self.plot_type, plotlist=self.plotlist, footprint_use_background=footprint_use_background)

        print 'Optimizing for best az/el...'
        print 'Core location is at (%3.1f, %3.1f)' % (self.shower_core[0], self.shower_core[1])
        optimum = fmin(self.chi2Minimizer_azel, (plane_az, plane_el), (self.shower_core, self.antenna_positions, self.pulse_delays, self.pulse_delay_uncertainties, None, self.verbose), xtol=1e-5, ftol=1e-5, full_output=True)

        # Get optimal curve fit
        (bestAz, bestEl) = optimum[0]
        (chi2, chi2_radio, ndf_radio, axisDistance, shower_plane_pulse_delays, fitted_params, fit_values, fitted_param_errors) = self.fit_chi2_and_curve_params(self.shower_core, bestAz, bestEl, self.antenna_positions, self.pulse_delays, uncertainties=self.pulse_delay_uncertainties, verbose=self.verbose)

        # Plot fit curve with data
        # Plot curve with parameters and errors as text.
        # Make in-plot text
        if self.save_plots:
            figtext = self.makePlotDescriptionText(chi2_radio, ndf_radio, fitted_params, fitted_param_errors)
            fig = self.plotFitCurveWithData(axisDistance, shower_plane_pulse_delays, self.pulse_delay_uncertainties, fit_values, stationList=stationList, stationStartIndex=stationStartIndex, in_plot_description=figtext)
            if not self.plot_publishable and self.plot_title:
                title = 'LDF-fitted core'
                if clockOffsetSearch:
                    title += '; shift '
                    for station in clockOffsetSearch.keys():
                        title += station + ': %2.2f' % shiftsPerStation[station]
                plt.title(title)
            self.save_plot(fig, 'wavefront_fit_hyperbolic')

        # store fit parameters
        self.fitParams = list(fitted_params)
        self.fitParamErrors = fitted_param_errors
        self.fitChi2 = chi2_radio / ndf_radio
        self.fitAzEl = (pytmf.rad2deg(bestAz), pytmf.rad2deg(bestEl))
        d_axisDistance = axisDistance
        d_showerPlanePulseDelays = shower_plane_pulse_delays
        d_pulseDelayUncertainties = self.pulse_delay_uncertainties
        d_evaluatedFitValues = fit_values

        # Save all fit parameters to output dict
        self.wavefront_output = {'fitPlaneWave': (plane_az, plane_el),
                                 'fitParams': self.fitParams,
                                 'fitParamErrors': self.fitParamErrors,
                                 'fitChi2': self.fitChi2,
                                 'fitAzEl': self.fitAzEl,
                                 'showerCore': self.shower_core,
                                 'stationList': stationList,
                                 'Npoints': len(self.pulse_delays),
                                 'clockShifts': shiftsPerStation,
                                 'chi2ForClockShifts': clockOffsetSearch,
                                 'axisDistance': d_axisDistance,
                                 'showerPlanePulseDelays': d_showerPlanePulseDelays,
                                 'pulseDelayUncertainties': d_pulseDelayUncertainties,
                                 'evaluatedFitValues': d_evaluatedFitValues
                                  }

        if self.verbose > 0:
            print 'Wavefront output: '
            print self.wavefront_output

