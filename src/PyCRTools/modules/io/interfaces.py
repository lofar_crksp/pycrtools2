import pycrtools as cr
from pycrtools import metadata as md


class IOInterface(object):
    """Base class for data IO.

    All functions or methods that use IO should expect to interface with
    objects of this type and all classes that implement IO should derive
    from this base class and overload it's functions.
    """

    def __init__(self, blocksize, block):
        # Store blocksize for readout
        self._blocksize = blocksize

        # Current block number
        self._block = block

    def setAntennaSelection(self, selection):
        """Sets the antenna selection used in subsequent calls to
        `getAntennaPositions`, `getFFTData`, `getTimeseriesData`.

        Required Arguments:

        =========== =================================================
        Parameter   Description
        =========== =================================================
        *selection* Either Python list with index of the antenna as
                    known to self (integers (e.g. ``[1, 5, 6]``))
                    Or list of IDs to specify a LOFAR dipole
                    (e.g. ``['142000005', '3001008']``)
        =========== =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises a `ValueError` if antenna selection cannot be set
        to requested value (e.g. specified antenna not in file).

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def setFrequencySelection(self, frequencies):
        """Sets the frequency selection used in subsequent calls to
        `getFFTData`.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *frequencies* :class:`~pycrtools.FloatVector` of frequencies
                      in Hz
        ============= =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises a `ValueError` if frequency selection
        cannot be set to requested values (e.g. specified frequency is not
        available for this blocksize.)

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def setFrequencyRange(self, fmin, fmax):
        """Sets the frequency selection used in subsequent calls to
        `getFFTData`. The selection set is all frequencies available
        for the set blocksize in the range [fmin,fmax].

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *fmin*        minimum frequency in Hz
        *fmax*        maximum frequency in Hz
        ============= =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises a `ValueError` if frequency selection
        cannot be set to requested values (e.g. all frequencies out of
        range)

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def setFrequencyRangeByIndex(self, nfmin, nfmax):
        """Sets the frequency selection used in subsequent calls to
        `getFFTData`.
        If **frequencies** is the array of frequencies available for the
        selected blocksize, then subsequent calls to `getFFTData` will
        return data corresponding to frequencies[nfmin:nfmax].

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *nfmin*       minimum frequency as index into frequency array
        *nfmax*       maximum frequency as index into frequency array
        ============= =================================================

        Output:
        This method does not return anything.

        Raises:
        It raises an `IndexError` if frequency selection cannot be set
        to requested values (e.g. index out of range)

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def getFrequencies(self):
        """Returns the frequencies that are applicable to the FFT data

        Output:
        This method returns a FloatVector with the selected frequencies
        in Hz.
        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def getRelativeAntennaPositions(self):
        """Returns relative antenna positions for selected antennas, or all
        antennas if no selection was applied.

        Output:
        a two dimensional array containing the Cartesian position of
        each antenna in meters in local coordinates from a predefined
        center.
        So that if `a` is the returned array `a[i]` is an array of
        length 3 with positions (x,y,z) of antenna i.
        """
        raise NotImplementedError

    def getITRFAntennaPositions(self):
        """Returns antenna positions for selected antennas, or all
        antennas if no selection was applied.

        Output:
        a two dimensional array containing the ITRF position of
        each antenna in meters in local coordinates from a predefined
        center.
        So that if `a` is the returned array `a[i]` is an array of
        length 3 with positions (x,y,z) of antenna i.
        """
        raise NotImplementedError

    def getFFTData(self, data, block):
        """Writes FFT data for selected antennas to data array.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write FFT data to.
        *block*       index of block to return data from.
        ============= =================================================

        Output:
        a two dimensional array containing the FFT data of the
        specified block for each of the selected antennae and
        for the selected frequencies.
        So that if `a` is the returned array `a[i]` is an array of
        length (number of frequencies) of antenna i.

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def getTimeseriesData(self, data, block):
        """Returns timeseries data for selected antennas.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *data*        data array to write timeseries data to.
        *block*       index of block to return data from.
        ============= =================================================

        Output:
        a two dimensional array containing the timeseries data of the
        specified block for each of the selected antennae.
        So that if `a` is the returned array `a[i]` is an array of
        length blocksize of antenna i.

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def getReferencePosition(self):
        """Returns reference position used for antenna position
        coordinate system.

        Output:
        a FloatVector with (lon,lat,height) in (rad,rad,m) of the WGS84
        position of the center used for the antenna position coordinate
        system.
        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")

    def read(self, key, data, *args, **kwargs):
        """Generic read function supporting keyword arguments.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *key*         Data type to read.
        *data*        array to write data to.
        ============= =================================================

        """
        raise NotImplementedError("You are attempting to call a method that has not been implemented for this subclass of the IO interface.")


class TBBDataInterface(IOInterface):
    """
    Define interface for TBB data files. 
    """

    def _notImplemented(self):
        raise NotImplementedError
    
    def selectedDipoles(self):
        raise NotImplementedError

    def info(self, verbose=True, show=True):
        """Display some information about the file. Short and long versions (verbose=False/True)
        """
        # Selecting a subset of keys.
        if verbose:
            key = self.keys()
            key = sorted([k for k in key if 'EMPTY' not in k and '_DATA' not in k])
        else:
            key = ['FILENAME', 'TIME_HR', 'DATA_SIZE', 'DATA_SIZE_TIME', 'STATION_NAME', 'NOF_DIPOLE_DATASETS']

        if self['ANTENNA_SET'] == 'UNDEFINED':
            print "WARNING: ANTENNA_SET == UNDEFINED cannot read antenna positions."
            key = [k for k in key if 'POSITION' not in k]

        output = '[TBB_Timeseries] Summary of object properties'
        if show:
            print output.strip()

        # For the print out format.
        maxii = 0
        for i in range(len(key)):
            maxii = max([maxii, len(key[i])])
        stringlength = maxii + 5
        und = ''

        # Loop over the selected keys.
        for k in key:
            s = ""
            if k == 'DATA_SIZE_TIME' and not(verbose):
                s = k + ' ' * (stringlength - len(k)) + ' : ' + str(cr.hArray(self['DATA_LENGTH']).median()[0] * self['SAMPLE_INTERVAL'][0]) + ' s'
                if show:
                    print s
                output += '\n' + s
                continue

            ss = k + ' ' * (stringlength - len(k)) + ' : '

            if k == 'DATA_SIZE' and not(verbose):
                s = ss + str(cr.hArray(self['DATA_LENGTH']).median()[0]) + ' Samples ( ' + str(cr.hArray(self['DATA_LENGTH']).median()[0] / self['BLOCKSIZE']) + ' BLOCKS, each of ' + str(self['BLOCKSIZE']) + ' Samples) '
                if show:
                    print s 
                output += '\n' + s
                continue
            if k == 'NOF_DIPOLE_DATASETS' and not(verbose):
                s = ss + str(self[k]) + '  ( ' + str(self['NOF_SELECTED_DATASETS']) + '  NOF_SELECTED_DATASETS ) '
                if show:
                    print s
                output += '\n' + s
                continue

            try:
                if isinstance(self[k], type([0, 0])) and len(self[k]) > 7:
                    if all(x == self[k][0] for x in self[k]):
                        if verbose:
                            s = ss + '[ ' + str(self[k][0]) + ', ...] x' + str(len(self[k])) + ' with ' + str(type(self[k][0]))
                        else:
                            s = ss + str(self[k][0])
                    else:
                        s = ss + str(self[k][:3] + ['...'] + self[k][-3:])
                else:
                    if isinstance(self[k], (cr.core.hftools._hftools.ComplexArray, cr.core.hftools._hftools.IntArray, cr.core.hftools._hftools.BoolArray, cr.core.hftools._hftools.FloatArray, cr.core.hftools._hftools.StringArray)):
                        s = ss + str(self[k].__repr__(maxlen=10))
                    else:
                        if self[k] == 'UNDEFINED':
                            und += k + ' , '
                            continue
                        else:
                            s = ss + str(self[k])
                if show:
                    print s
                output += '\n' + s

            except IOError:
                pass
        if len(und) > 0:
            s = 'These keywords are UNDEFINED : [' + str(und) + ']'
            if show:
                print s
            output += s

        if not show:
            return output.strip()

    def __repr__(self):
        """Display summary when printed.
        """
        return self.info(False, False)

    def keys(self, excludedata=False):
        """Returns list of valid keywords.
        """
        return [k for k in self._keyworddict.keys() if not k[-5:] == "_DATA"] if excludedata else self._keyworddict.keys()

    def items(self, excludedata=False):
        """Return list of keyword/content tuples of all header variables
        """

        lst = []

        for k in self.keys(excludedata):
            try:
                lst.append((k, self._keyworddict[k]() if hasattr(self._keyworddict[k], "__call__") else self._keyworddict[k]))
            except IOError:
                pass

        return lst

    def getHeader(self):
        """Return a dict with keyword/content pairs for all header variables."""
        return dict(self.items(excludedata=True))

    def next(self, step=1):
        """Advance to next block.
        *step* = 1 - advance by 'step' blocks (optional)
        """
        self._block += step

    def __getitem__(self, *keys):
        """Implements keyword access.
        """
        # If multiple keywords are provided, return a list of results
        if isinstance(keys[0], tuple):
            return [self[k] for k in keys[0]]
        else:
            key = keys[0]

        if key not in self.keys():
            raise KeyError("Invalid keyword: " + key)
        else:
            if hasattr(self._keyworddict[key], "__call__"):
                return self._keyworddict[key]()
            else:
                return self._keyworddict[key]

    setable_keywords = set(["BLOCKSIZE", "BLOCK", "SELECTED_DIPOLES", "ANTENNA_SET"])

    def __setitem__(self, key, value):
        if key not in self.setable_keywords:
            raise KeyError("Invalid keyword '" + str(key) + "' - vailable keywords: " + str(list(self.setable_keywords)))

        elif key is "BLOCKSIZE":
            self._blocksize = value
            self._initSelection()
        elif key is "BLOCK":
            self._block = value
        elif key is "SELECTED_DIPOLES":
            self.setAntennaSelection(value)
        elif key is "ANTENNA_SET":
            self.antenna_set = value
        else:
            raise KeyError(str(key) + " cannot be set. Available keywords: " + str(list(self.setable_keywords)))

    def __contains__(self, key):
        """Allows inquiry if key is implemented.
        """

        return key in self.keys()

    def getTimeData(self, data=None, block=-1):
        """Calculate time axis depending on sample frequency and
        blocksize (and later also time offset). Create a new array, if
        none is provided, otherwise put data into array.
        """

        if not data:
            data = self.empty("TIME_DATA")

        block = cr.asval(block)

        if block < 0:
            block = self._block
        else:
            self._block = block

        data.fillrange(self["BLOCK"] * self["BLOCKSIZE"] * cr.asval(self["SAMPLE_INTERVAL"]), cr.asval(self["SAMPLE_INTERVAL"]))
        return data


    def shiftTimeseriesData(self, sample_offset=0):
        """Shifts timeseries data for selected antennas.

        Required Arguments:

        =============== =================================================
        Parameter       Description
        =============== =================================================
        *sample_offset* Number of samples to offset timeseries data.
        =============== =================================================

        """
        self._shift = sample_offset

  

    def getStationCenterRelativeAntennaPosition(self):
        """Returns antenna positions relative to the station center for selected
        antennas, or all antennas if no selection was applied.

        Output:
        a two dimensional array containing the Cartesian position of
        each antenna in meters in local coordinates from the station center
        center.
        So that if `a` is the returned array `a[i]` is an array of
        length 3 with positions (x,y,z) of antenna i.
        """

        return md.get("StationCenterRelativeAntennaPosition", self.selectedDipoles(), self.antenna_set, True)

    def getLofarCenterRelativeAntennaPosition(self):
        """Returns antenna positions relative to the LOFAR center (e.g. antenna 0 of station CS002) for selected
        antennas, or all antennas if no selection was applied.

        Output:
        a two dimensional array containing the Cartesian position of
        each antenna in meters in local coordinates from the station center
        center.
        So that if `a` is the returned array `a[i]` is an array of
        length 3 with positions (x,y,z) of antenna i.
        """

        return md.get("LofarCenterRelativeAntennaPosition", self.selectedDipoles(), self.antenna_set, True)

    def getItrfAntennaPosition(self):
        """Returns antenna positions for selected antennas, or all
        antennas if no selection was applied.

        Output:
        a two dimensional array containing the Cartesian position of
        each antenna in meters in ITRF coordinates.
        So that if `a` is the returned array `a[i]` is an array of
        length 3 with positions (x,y,z) of antenna i.
        """

        return md.get("ItrfAntennaPosition", self.selectedDipoles(), self.antenna_set, True)

    def getFrequencies(self, block=-1):
        """Returns the frequencies that are applicable to the FFT data

        Arguments:
        *block* = -1 - Is without function, just for compatibility.

        Return value:
        This method returns a FloatVector with the selected frequencies
        in Hz.

        """

        # Get empty array of correct size
        frequencies = self.empty("FREQUENCY_DATA")

        # Calculate sample frequency in Hz
        cr.hFFTFrequencies(frequencies, self["SAMPLE_FREQUENCY"][0], self["NYQUIST_ZONE"][0])

        return frequencies

    def getClockOffset(self):
        """Return clock offset.
        """
        # self["TIME"][0] contains the Unix time (not event-ID time)
        return [md.getClockCorrection(s, antennaset=self["ANTENNA_SET"], time=self["TIME"][0]) for s in self["STATION_NAME"]]

    def read(self, key, data, *args, **kwargs):
        """Generic read function supporting keyword arguments.

        Required Arguments:

        ============= =================================================
        Parameter     Description
        ============= =================================================
        *key*         Data type to read, one of:
                      *TIMESERIES_DATA*
                      *FFT_DATA*
                      *FREQUENCY_DATA*
                      *TIME_DATA*
        *data*        array to write data to.
        ============= =================================================

        """

        if key == "TIMESERIES_DATA":
            return self.getTimeseriesData(data, *args, **kwargs)
        elif key == "TIME_DATA":
            return self.getTimeData(data, *args, **kwargs)
        elif key == "FREQUENCY_DATA":
            return data.copy(self.getFrequencies(*args, **kwargs))
        elif key == "FFT_DATA":
            return self.getFFTData(data, *args, **kwargs)
        else:
            raise KeyError("Unknown key: " + str(key))

    def empty(self, key):
        """Return empty array for keyword data.
        Known keywords are: "TIMESERIES_DATA", "TIME_DATA", "FREQUENCY_DATA", "FFT_DATA".
        """

        if key == "TIMESERIES_DATA":
            return cr.hArray(float, dimensions=(self['NOF_SELECTED_DATASETS'], self['BLOCKSIZE']), name="E-Field(t)", units=("", "Counts"))
        elif key == "TIME_DATA":
            return cr.hArray(float, self["BLOCKSIZE"], name="Time", units=("", "s"))
        elif key == "FREQUENCY_DATA":
            return cr.hArray(float,  self['BLOCKSIZE'] / 2 + 1, name="Frequency", units=("", "Hz"))
        elif key == "FFT_DATA":
            return cr.hArray(complex, dimensions=(self['NOF_SELECTED_DATASETS'], self['BLOCKSIZE'] / 2 + 1), name="fft(E-Field)", xvalues=self["FREQUENCY_DATA"], logplot="y")
        else:
            raise KeyError("Unknown key: " + str(key))

    def getTimeseriesData(self, data, block=-1, sample_offset=0, datacheck=False):
        raise NotImplementedError
    
    def setAntennaSelection(self, selection):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError
