import os
import glob
import psycopg2
from optparse import OptionParser

# Parse commandline options
parser = OptionParser()
parser.add_option("--host", default=None, help="PostgreSQL host.")
parser.add_option("--user", default=None, help="PostgreSQL user.")
parser.add_option("--password", default=None, help="PostgreSQL password.")
parser.add_option("--dbname", default=None, help="PostgreSQL dbname.")
parser.add_option("--directory", default=None, help="Top level directory to clean.")

(options, args) = parser.parse_args()

conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)

# Create cursor
c = conn.cursor()

c.execute("""SELECT e.status, e.eventid, s.stationname FROM events AS e, event_datafile AS ed, datafile_station AS ds, stations AS s WHERE e.eventid=ed.eventid AND ed.datafileid=ds.datafileid AND ds.stationid=s.stationid AND s.status!='GOOD' OR e.status!='CR_FOUND';""")

for rec in c.fetchall():
    if rec[0] != "CR_FOUND":
        obsolete = glob.glob(os.path.join(options.directory,str(rec[0])).rstrip("/")+"/*{0}*.npy".format(rec[1]))
    else:
        obsolete = glob.glob(os.path.join(options.directory,str(rec[1])).rstrip("/")+"/*{1}*{2}*.npy".format(rec[0], rec[1]))

    for f in obsolete:
        try:
            print f
#            os.remove(f)
        except:
            print "error: cannot remove", f

