import os
import optparse
import subprocess 
import psycopg2

# Parse commandline options
parser = optparse.OptionParser()

parser.add_option("--host", default=None, help="PostgreSQL host.")
parser.add_option("--user", default=None, help="PostgreSQL user.")
parser.add_option("--password", default=None, help="PostgreSQL password.")
parser.add_option("--dbname", default=None, help="PostgreSQL dbname.")
parser.add_option("--skip-conex", default=False, action="store_true")
parser.add_option("--skip-coreas", default=False, action="store_true")
parser.add_option("--skip-analysis", default=False, action="store_true")

(options, args) = parser.parse_args()

# Connect to database
conn = psycopg2.connect(host=options.host, user=options.user, password=options.password, dbname=options.dbname)

# Create cursor
c = conn.cursor()

# find events to start simulations for
sql = "SELECT eventid FROM events WHERE (simulation_status='DESIRED' OR simulation_status='CONEX_STARTED' OR simulation_status='CONEX_DONE' OR simulation_status='COREAS_STARTED') ORDER BY eventid" #  OR simulation_status='COREAS_DONE'
c.execute(sql)

events = [int(e[0]) for e in c.fetchall()]

skip = ""
if options.skip_conex:
    skip += "--skip-conex "
if options.skip_coreas:
    skip += "--skip-coreas "
if options.skip_analysis:
    skip += "--skip-analysis "

for eventID in events:
    print eventID
    subprocess.call("python " + os.environ["LOFARSOFT"] + "/src/PyCRTools/pipelines/cr_simulation.py --host={1} --user={2} --password={3} --dbname={4} --id={0} {5}".format(eventID, options.host, options.user, options.password, options.dbname, skip), shell=True)

# Close connection to database
conn.close()

proc = subprocess.Popen(['chgrp -R lofar /vol/astro7/lofar/sim/pipeline/*'], shell=True)
proc.wait()
